# 7olyan-edu

## Tasks 

### First task
1. Make sure that you have installed all tools and server runs properly
   1. Install
      1. Docker desktop
      2. Node
      3. Postman
   1. Understanding how endpoints work (i disabled all security for this project so you have only public endpoints)
      1. You can access any endpoint from http://localhost:8080/*, so first read what does _endpoint_ actually means
      2. Next determine main endpoint that you can access, you can find them
         inside ``server/src/main/java/com/chystiakov/react/controller/*`` just go through controllers, there you can
         find ``@Path("/users")``, so we have http://localhost:8080/users for example, there are also a lot of other
         methods and each have own endpoint that depends on http methods(also recommend to rad about it and http
         responses code)
      3. Next try to understand what is postman (maybe watch some tutors' ect.) and try to do some requests from this
         app to your server endpoints

