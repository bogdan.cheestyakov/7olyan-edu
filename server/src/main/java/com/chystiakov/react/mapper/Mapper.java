package com.chystiakov.react.mapper;

import com.chystiakov.react.model.User;
import com.chystiakov.react.model.UserEditDto;
import com.chystiakov.react.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class Mapper {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserEditDto toDto(User user) {
        UserEditDto userDto = new UserEditDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(null);
        userDto.setRptPassword(null);
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setBirthday(user.getBirthday());
        userDto.setRole(user.getRole());
        return userDto;
    }

    public User toUser(UserEditDto userDto) {
        User user = userService.findUserById(userDto.getId());
        if (userDto.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setBirthday(userDto.getBirthday());
        user.setRole(userDto.getRole());
        return user;
    }

}
