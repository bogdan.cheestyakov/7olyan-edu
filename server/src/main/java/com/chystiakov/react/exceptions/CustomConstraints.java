package com.chystiakov.react.exceptions;

import lombok.Getter;

import java.util.List;

@Getter
public class CustomConstraints extends RuntimeException {
    private final List<String> errors;

    public CustomConstraints(List<String> errors) {
        this.errors = errors;
    }

}
