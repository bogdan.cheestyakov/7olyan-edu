package com.chystiakov.react.exceptions;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        String path, fieldName;
        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
            path = constraintViolation.getPropertyPath().toString();
            fieldName = path.substring(path.lastIndexOf(".") + 1);
            errors.add("\n" + fieldName + " " + constraintViolation.getMessage());
        }
        return Response.status(Response.Status.CONFLICT).entity(Map.of("errors", errors)).build();
    }
}
