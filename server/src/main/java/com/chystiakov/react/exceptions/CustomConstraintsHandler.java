package com.chystiakov.react.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Map;

public class CustomConstraintsHandler implements ExceptionMapper<CustomConstraints> {

    @Override
    public Response toResponse(CustomConstraints exception) {
        return Response
                .status(Response.Status.CONFLICT)
                .entity(Map.of("errors", exception.getErrors()))
                .build();
    }
}
