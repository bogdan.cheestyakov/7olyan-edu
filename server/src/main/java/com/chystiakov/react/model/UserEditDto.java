package com.chystiakov.react.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Date;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class UserEditDto implements Serializable {

    @NotNull
    private Long id;

    @NotEmpty
    @NotNull
    private String login;

    private String password;

    private String rptPassword;

    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message = "enter correct email")
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String email;

    @Pattern(regexp = "[a-zA-Z]+", message = "enter your real first name")
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String firstName;

    @Pattern(regexp = "[a-zA-Z]+", message = "enter your real last name")
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String lastName;

    @NotNull(message = "required field")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = DateSerializer.class)
    private Date birthday;

    @NotNull(message = "required field")
    private Role role;
}
