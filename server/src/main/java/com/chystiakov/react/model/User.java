package com.chystiakov.react.model;

import com.chystiakov.react.conversionService.DateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Date;

@Getter
@Entity
@NoArgsConstructor
@ToString
@Setter
@EqualsAndHashCode
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "LOGIN", nullable = false, unique = true)
    @Pattern(regexp = "[\\w.]+", message = "use correct symbols for login")
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String login;

    @Column(name = "PASSWORD", nullable = false)
    @NotEmpty(message = "required field")
    @Pattern(regexp = ".{4,32}", message = "password must not be shorter than 4 and more than 32 characters")
    @NotNull(message = "required field")
    private String password;

    @Transient
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String rptPassword;

    @Column(name = "EMAIL", nullable = false, unique = true)
    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$",
            message = "enter correct email")
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String email;

    @Column(name = "FIRSTNAME", nullable = false)
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String firstName;

    @Column(name = "LASTNAME", nullable = false)
    @NotEmpty(message = "required field")
    @NotNull(message = "required field")
    private String lastName;

    @Column(name = "BIRTHDAY", nullable = false)
    @NotNull(message = "required field")
    @JsonSerialize(using = DateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @ManyToOne
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")
    private Role role;
}