package com.chystiakov.react.controller;


import com.chystiakov.react.exceptions.CustomConstraints;
import com.chystiakov.react.mapper.Mapper;
import com.chystiakov.react.model.Role;
import com.chystiakov.react.model.User;
import com.chystiakov.react.model.UserEditDto;
import com.chystiakov.react.service.role.RoleService;
import com.chystiakov.react.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@Validated
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class ControllerForOrdinaryUsers {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;


    @Autowired
    private Mapper mapper;

    @POST
    @Path("/register")
    public UserEditDto registerUser(@Valid User user) {
        if (!user.getPassword().equals(user.getRptPassword())) {
            throw new CustomConstraints(List.of("passwords are not equal"));
        }
        Role role = roleService.findRoleByName("ROLE_USER");
        user.setRole(role);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return mapper.toDto(userService.saveUser(user));
    }

}
