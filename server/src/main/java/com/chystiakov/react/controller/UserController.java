package com.chystiakov.react.controller;

import com.chystiakov.react.exceptions.CustomConstraints;
import com.chystiakov.react.mapper.Mapper;
import com.chystiakov.react.model.User;
import com.chystiakov.react.model.UserEditDto;
import com.chystiakov.react.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Validated
@Path("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private Mapper mapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GET
    public Collection<UserEditDto> getAllUsers() {
        return userService.findAllUsers();
    }

    @GET
    @Path("/{id}")
    public UserEditDto getUserForEdit(@Min(1) @PathParam("id") Long id) {
        User user = userService.findUserById(id);
        if (user == null) {
            return new UserEditDto();
        }
        return mapper.toDto(userService.findUserById(id));
    }

    @Path("/{id}")
    @DELETE
    public Object deleteUser(@Min(1) @PathParam("id") Long id) {
        User user = userService.findUserById(id);
        if (user == null) {
            return Map.of("deleted", Boolean.FALSE);
        }
        userService.removeUser(user);
        return Map.of("deleted", Boolean.TRUE);
    }

    @PUT
    public UserEditDto editUser(@Valid UserEditDto userEditDto) {
        if (userEditDto.getPassword() != null
                && !userEditDto.getPassword().equals(userEditDto.getRptPassword())) {
            throw new CustomConstraints(List.of("passwords are not equal"));
        }
        return userService.updateUser(userEditDto);
    }

    @POST
    public UserEditDto addUser(@Valid User user) {
        System.out.println(user);
        if (!user.getPassword().equals(user.getRptPassword())) {
            throw new CustomConstraints(List.of("passwords are not equal"));
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return mapper.toDto(userService.saveUser(user));
    }

}
