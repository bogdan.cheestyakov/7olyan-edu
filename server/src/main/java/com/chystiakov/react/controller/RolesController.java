package com.chystiakov.react.controller;

import com.chystiakov.react.model.Role;
import com.chystiakov.react.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@RestController
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Validated
@Path("/roles")
public class RolesController {

    @Autowired
    private RoleService roleService;

    @GET
    public Collection<Role> getAllRoles() {
        return roleService.findAllRoles();
    }
}
