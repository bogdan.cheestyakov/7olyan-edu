package com.chystiakov.react.service.role;


import com.chystiakov.react.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> findAllRoles();

    Role findRoleById(Long id);

    Role findRoleByName(String name);
}
