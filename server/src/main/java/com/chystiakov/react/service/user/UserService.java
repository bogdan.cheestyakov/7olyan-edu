package com.chystiakov.react.service.user;


import com.chystiakov.react.model.User;
import com.chystiakov.react.model.UserEditDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<UserEditDto> findAllUsers();

    User saveUser(User user);

    UserEditDto updateUser(UserEditDto user);

    User findUserByLogin(String login);

    User findUserById(Long id);

    void removeUser(User id);

    User findUserByEmail(String email);
}
