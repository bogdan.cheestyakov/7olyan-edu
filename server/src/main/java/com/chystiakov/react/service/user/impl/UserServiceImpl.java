package com.chystiakov.react.service.user.impl;

import com.chystiakov.react.exceptions.CustomConstraints;
import com.chystiakov.react.mapper.Mapper;
import com.chystiakov.react.model.User;
import com.chystiakov.react.model.UserEditDto;
import com.chystiakov.react.repository.user.UserRepository;
import com.chystiakov.react.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private Mapper mapper;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findUserByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }
        Collection<SimpleGrantedAuthority> authorities = Collections.singleton(new SimpleGrantedAuthority(user.getRole().getName()));
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
    }

    @Override
    public List<UserEditDto> findAllUsers() {
        return userRepository.findAll().stream().map(x -> mapper.toDto(x)).collect(Collectors.toList());
    }


    private boolean isEmailUnique(User email) {
        User user = findUserByEmail(email.getEmail());
        return user != null && !user.getId().equals(email.getId());
    }

    @Override
    public UserEditDto updateUser(UserEditDto userEditDto) {
        List<String> errors = new ArrayList<>();
        User user = mapper.toUser(userEditDto);
        if (isEmailUnique(user)) {
            errors.add("\nuser with such email already exist");
        }
        if (errors.size() > 0) {
            throw new CustomConstraints(errors);
        }
        userRepository.save(user);
        return userEditDto;
    }

    private boolean isLoginUnique(User login) {
        return findUserByLogin(login.getLogin()) != null;
    }

    @Override
    public User saveUser(User user) {
        List<String> errors = new ArrayList<>();
        if (isLoginUnique(user)) {
            errors.add("\nuser with such login already exist");
        }
        if (isEmailUnique(user)) {
            errors.add("\nuser with such email already exist");
        }
        if (errors.size() > 0) {
            throw new CustomConstraints(errors);
        }
        userRepository.save(user);
        return user;
    }

    @Override
    public User findUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findUserById(id);
    }

    @Override
    public void removeUser(User t) {
        userRepository.delete(t);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }


}
