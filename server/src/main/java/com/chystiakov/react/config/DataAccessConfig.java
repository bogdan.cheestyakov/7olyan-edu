package com.chystiakov.react.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.chystiakov.react.repository")
public class DataAccessConfig {

    @Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }


    @Bean
    public BasicDataSource getDataSource(Environment env) {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(env.getRequiredProperty("db.driver"));
        basicDataSource.setUrl(env.getRequiredProperty("db.url"));
        basicDataSource.setUsername(env.getRequiredProperty("db.username"));
        basicDataSource.setPassword(env.getRequiredProperty("db.password"));
        basicDataSource.setInitialSize(Integer.parseInt(env.getRequiredProperty("db.initialSize")));
        basicDataSource.setMaxTotal(Integer.parseInt(env.getRequiredProperty("db.maxTotal")));
        basicDataSource.setMaxIdle(Integer.parseInt(env.getRequiredProperty("db.maxIdle")));
        basicDataSource.setMinIdle(Integer.parseInt(env.getRequiredProperty("db.minIdle")));
        return basicDataSource;
    }

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory(BasicDataSource dataSource,
                                                                Environment env) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.chystiakov.react.model");
        Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect",
                env.getRequiredProperty("hibernate.dialect"));
        jpaProperties.put("hibernate.hbm2ddl.auto",
                env.getRequiredProperty("hibernate.hbm2ddl.auto"));
        jpaProperties.put("hibernate.show_sql",
                env.getRequiredProperty("hibernate.show_sql"));
        jpaProperties.put("hibernate.format_sql",
                env.getRequiredProperty("hibernate.format_sql"));
        jpaProperties.put("hibernate.hbm2ddl.import_files",
                env.getRequiredProperty("hibernate.hbm2ddl.import_files"));
        jpaProperties.put("javax.persistence.validation.mode",
                env.getRequiredProperty("javax.persistence.validation.mode"));
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        return entityManagerFactoryBean;
    }

}
