package com.chystiakov.react.config;


import com.chystiakov.react.controller.ControllerForOrdinaryUsers;
import com.chystiakov.react.controller.RolesController;
import com.chystiakov.react.controller.UserController;
import com.chystiakov.react.exceptions.CustomConstraintsHandler;
import com.chystiakov.react.exceptions.ExceptionHandler;
import com.chystiakov.react.mapper.Mapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.validation.JAXRSBeanValidationInInterceptor;
import org.apache.cxf.jaxrs.validation.JAXRSBeanValidationOutInterceptor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;
import java.util.List;

@Configuration
@ComponentScan(basePackages = {
        "com.chystiakov.react.security",
        "com.chystiakov.react.service"
})
@EnableWebMvc
@PropertySource("classpath:application.properties")
@ComponentScan(basePackageClasses = DataAccessConfig.class)
public class RootConfig implements WebMvcConfigurer {

    @ApplicationPath("/")
    public static class JaxRsApiApplication extends Application {
    }

    @Bean
    public ExceptionHandler getExceptionHandler() {
        return new ExceptionHandler();
    }


    @Bean
    public JaxRsApiApplication jaxRsApiApplication() {
        return new JaxRsApiApplication();
    }

    @Bean
    public UserController userController() {
        return new UserController();
    }

    @Bean
    public ControllerForOrdinaryUsers controllerForOrdinaryUsers() {
        return new ControllerForOrdinaryUsers();
    }

    @Bean
    public RolesController rolesController() {
        return new RolesController();
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public CustomConstraintsHandler customConstraintsHandler() {
        return new CustomConstraintsHandler();
    }

    @Bean
    @DependsOn("cxf")
    public Server jaxRsServer(ApplicationContext appContext) {
        JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance()
                .createEndpoint(jaxRsApiApplication(), JAXRSServerFactoryBean.class);
        factory.setServiceBeans(List.of(userController(), controllerForOrdinaryUsers(),
                rolesController()));
        factory.setProviders(List.of(jsonProvider(), getExceptionHandler(),
                customConstraintsHandler()));
        factory.setInInterceptors(List.of(new JAXRSBeanValidationInInterceptor()));
        factory.setOutInterceptors(List.of(new JAXRSBeanValidationOutInterceptor()));
        return factory.create();
    }


    @Bean
    public Mapper mapper() {
        return new Mapper();
    }

    @Bean
    public JacksonJsonProvider jsonProvider() {
        return new JacksonJsonProvider();
    }
}
