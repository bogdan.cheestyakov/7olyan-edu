INSERT INTO ROLE(NAME) VALUES ('ROLE_ADMIN');
INSERT INTO ROLE(NAME) VALUES ('ROLE_USER');
INSERT INTO USER(LOGIN, PASSWORD, EMAIL, FIRSTNAME, LASTNAME, BIRTHDAY, ROLE_ID) VALUES ('admin', '$2a$10$c0FAtQZl4CkD9SVas2mFAeb/D0XCpTBJo9UWltwGD7VeNYdNBckc6', 'bohdan.chystiakov@nixsolutions.com', 'Bogdan', 'Chystiakov', parsedatetime('04-06-2002', 'dd-MM-yyyy'), 1);
INSERT INTO USER(LOGIN, PASSWORD, EMAIL, FIRSTNAME, LASTNAME, BIRTHDAY, ROLE_ID) VALUES ('user', '$2a$10$thBO9GnrlQ1pFaWHJGdve.MZKwZI6../Ds7WeeUfU3SnXL1P5l8da', 'someemail@gmail.com', 'Dan', 'Perepelica', parsedatetime('28-12-2001', 'dd-MM-yyyy'), 2);

